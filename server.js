"use strict"

const express = require("express"),
  app = express(), // Project's main application framework.
  layouts = require("express-ejs-layouts"),
  path = require("path");

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/ceep_iteration15",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
   }
);
mongoose.set("useCreateIndex", true);

// Set port to environment variable or 4000.
app.set("port", process.env.PORT || 4000);

// Set application to use EJS.
app.set("view engine", "ejs");

// Use Express Router.
const router = require("./routes/index");

// Set application to use Express EJS Layouts module.
app.use(layouts);

// Enable static assets to be served directly.
app.use(express.static("public"));

// Body parsing. Interprets incoming request bodies.
app.use(
  express.urlencoded({
    extended: false
  })
);
app.use(express.json());

// Enable PUT and DELETE HTTP methods.
const methodOverride = require("method-override");
app.use(
  methodOverride("_method", {
    methods: ["POST", "GET"]
  })
);

// Flash Messaging.
const expressSession = require("express-session"),
  cookieParser = require("cookie-parser"),
  connectFlash = require("connect-flash");
app.use(cookieParser("secret_passcode"));
app.use(expressSession({
  secret: "secret_passcode",
  cookie: {
    maxAge: 4000000
  },
  resave: false,
  saveUninitialized: false
}));
app.use(connectFlash());

// Database models.
const User = require("./models/user"),
  Course = require("./models/course"),
  StudyBook = require("./models/studyBook"),
  Question = require("./models/question");

// User authentication.
const passport = require("passport");
app.use(passport.initialize());
app.use(passport.session());
passport.use(User.createStrategy());
// Uses Mongo document id as session key.
passport.serializeUser((user, done) => {
  done(null, user._id)
});
passport.deserializeUser((id, done) => {
  User.findById(id)
    .then((user, err) => {
      done(err, user);
    })
});

app.use((req, res, next) => {
  res.locals.loggedIn = req.isAuthenticated();
  res.locals.currentUser = req.user;
  res.locals.flashMessages = req.flash();
  next();
});

app.use("/", router);

// Serve production build files.
app.use(express.static(path.join(__dirname, "/client/build")));
app.get('/*', function (req, res) {
   res.sendFile(path.join(__dirname, "/client/build", "index.html"));
 });

// Add error middlware. Must come after other middleware and routers.
const errorController = require("./controllers/errorController");
app.use(errorController.pageNotFoundError);
app.use(errorController.internalServerError);

app.listen(app.get("port"), () => {
  console.log(`Server running at http://localhost:${app.get("port")}`);
});
