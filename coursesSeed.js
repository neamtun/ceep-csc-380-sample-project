"use strict"

const mongoose = require("mongoose"),
  Course = require("./models/course"),
  StudyBook = require("./models/studyBook"),
  { createStudyBooks, createStudyBookArray } = require("./studyBooksSeed");

/*
 * Creates courses from a list of JSON representations.
 * Synchronously creates courses.
 * Synchronously updates the courses to reference the study book ids.
 * These steps are done synchronously because they are required to be completed before
 * moving on.
 * Returns an array of the newly created course ids.
 * The calling method can use the course ids because the courses are guaranteed
 * to have been created.
 * course: array of JSON objects of study books and corresponding study books.
 * return: array of course ids.
 */
const createCourses = async(courses) => {
  let commands = [];
  let courseIds = [];

  courses.forEach(c => {
    commands.push(Course.create(c.course)
      .then(async(courseDoc) => {

        // These database operations much be synchronous too else code continues after Promise.all.
        await createStudyBooks(c.studyBooks)
          .then(async(studyBookIds) => {
            courseDoc.studyBooks = studyBookIds;

            // Update course with references to study book ids
            await courseDoc.save()
              .then(courseDoc => {
                console.log("Course:\n" + courseDoc);
                courseIds.push(courseDoc._id);
              })
          })
      }));
  });

  // Forces study books to be created synchronously.
  await Promise.all(commands);

  return courseIds;
}

/*
 * Get array of test courses objects.
 */
const getTestCourses = () => {
  return [
    {
      course: {
        title: "CSC 111",
        description: "Computer Science Principles",
        maxStudents: 40
      },
      studyBooks: createStudyBookArray(1, 3, 2, 3)
    },
    {
      course: {
        title: "CSC 112",
        description: "Data Structures",
        maxStudents: 30
      },
      studyBooks: createStudyBookArray(4, 6, 2, 3)
    },
    {
      course: {
        title: "CSC 380",
        description: "Web Development",
        maxStudents: 25
      },
      studyBooks: createStudyBookArray(7, 9, 2, 3)
    }
  ]
}

module.exports = {
  createCourses: createCourses,
  getTestCourses: getTestCourses
}
