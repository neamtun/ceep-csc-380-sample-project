"use strict"

const mongoose = require("mongoose"),
  User = require("./models/user"),
  Course = require("./models/course"),
  StudyBook = require("./models/studyBook"),
  Question = require("./models/question"),
  Submission = require("./models/submission"),
  { createCourses, getTestCourses } = require("./coursesSeed"),
  { getCourseIdsByTitles,
    insertCourseIds,
    createUsers,
    getTestUsers } = require("./usersSeed"),
  { populateCurrentUsers, createSubmissions } = require("./submissionsSeed");

/*
 * Delete all data for each model.
 * Synchronous to make sure data is deleted before the next operation.
 * models: array of models.
 */
const deleteModelData = async(models) => {
  let commands = [];

  models.forEach((model) => {
    commands.push(model.deleteMany()
      .then(() => {
        console.log(model.collection.collectionName + " data deleted");
      }));
  });

  await Promise.all(commands);
}

const getModels = () => {
  return [
    User,
    Course,
    StudyBook,
    Question,
    Submission
  ];
}

mongoose.connect(
  "mongodb://localhost:27017/ceep_iteration15",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  }
)

mongoose.connection;

let models = getModels();
let users = getTestUsers();
let courses = getTestCourses();

deleteModelData(models)
  .then(() => {
    createCourses(courses)
      .then(() => {
        let courseTitles = courses.map(course => {
          return course.course.title;
        });

        getCourseIdsByTitles(courseTitles).then(courseTitleToId => {
          users = insertCourseIds(users, courseTitleToId)

          createUsers(users).then(() => {

            populateCurrentUsers().then((users) => {
              createSubmissions(users).then(() => {
                mongoose.connection.close();
              })
            });
          });
        });
      });
  });
