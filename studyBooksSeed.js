"use strict"

const mongoose = require("mongoose"),
  StudyBook = require("./models/studyBook"),
  Question = require("./models/question");

/*
 * Creates all the question documents.
 * Returns an array of the newly created question ids.
 * Forces the questions to be created synchronously so if the ids are used in
 * the calling method the questions are guaranteed to have been created.
 * questions: array of JSON objects.
 * return: array of question ids.
*/
const createQuestions = async(questions) => {
  let commands = [];
  let questionIds = [];

  questions.forEach((q) => {
    commands.push(Question.create(
        {
          text: q.text,
          answer: q.answer
        }
      )
    );
  });

  await Promise.all(commands)
    .then(r => {
      r.forEach(s => {
        questionIds.push(s._id);
      })
      console.log("Questions:\n" + JSON.stringify(r));
    })
    .catch(error => {
      console.log(`ERROR: ${error}`);
    });

  return questionIds;
}

/*
 * Creates study books and questions from a list of JSON representations.
 * Synchronously creates the study books.
 * Synchronously creates the questions.
 * Synchronously updates the study books to reference the question ids.
 * These steps are done synchronously because they are required to be completed before
 * moving on.
 * Returns an array of the newly created study book ids.
 * The calling method can use the study book ids because the study books are guaranteed
 * to have been created.
 * studyBooks: array of JSON objects of study books and corresponding questions.
 * return: array of study book ids.
 */
const createStudyBooks = async(studyBooks) => {
  let commands = [];
  let studyBookIds = [];

  studyBooks.forEach(s => {
    commands.push(StudyBook.create(s.book)
      .then(async(studyBookDoc) => {

        // These database operations much be synchronous too else code continues after Promise.all.
        await createQuestions(s.questions)
          .then(async(questionIds) => {
            studyBookDoc.questions = questionIds;

            // Update study book with references to question ids.
            await studyBookDoc.save()
              .then(studyBookDoc => {
                console.log("StudyBook:\n" + studyBookDoc);
                studyBookIds.push(studyBookDoc._id);
              });
          });
      }));
    });

  // Forces study books to be created synchronously.
  await Promise.all(commands);

  return studyBookIds;
}

/*
 * Create test question object.
 * bookNum: study book number.
 * questionNum: question number.
 */
const createQuestionObj = (bookNum, questionNum) => {
  return {
    text: `Book ${bookNum} Question ${questionNum}`,
    answer: `answer${questionNum}`
  };
}

/*
 * Create test study book object.
 * bookNum: study book number.
 * numQuestions: number of questions to be in study book.
 */
const createStudyBookObj = (bookNum, numQuestions) => {
  return {
    book: {
      title: `Book ${bookNum}`
    },
    questions: [...Array(numQuestions)].map((_, i) => {
      return createQuestionObj(bookNum, i+1);
    })
  }
}

/*
 * Create array of test study book objects.
 * minBook: minimum book number.
 * maxBook: maximum book number.
 * minQuestions: minimum number of questions
 * maxQuestions: maximum number of questions.
 */
const createStudyBookArray = (minBook, maxBook, minQuestions, maxQuestions) => {
  return Array.from(new Array(maxBook - minBook + 1), (x,i) => i + minBook)
    .map(j => {
      return createStudyBookObj(j, Math.floor(Math.random() * (maxQuestions - minQuestions + 1) + minQuestions));
    });
}

module.exports = {
  createQuestions: createQuestions,
  createStudyBooks: createStudyBooks,
  createStudyBookArray: createStudyBookArray
}
