# CEEP CSC 380 Sample Project

Sample project developed for CSC 380 for Nick Neamtu's CEEP project.

## Requirements
* [Node.js](https://nodejs.org/en/)
* [MongoDB](https://docs.mongodb.com/manual/installation/) running on default port 27017

## How to use
* run `npm install` within the project directory to install dependencies

### Iterations 1-5
* run `npm start` within the project directory to start the Express server at port 3000

### Iteration 2 and on
* run `node seed.js` within the project directory to populate the MongoDB database

### Iterations 6 and on
* run `npm install` within the client directory to install dependencies for the React client
* run `npm run server` within the project directory to start the Express server at port 3000
* run `npm run client` within the project directory to start the React client at port 4000

### Production Build (Iteration 15)
* run `npm run build` within the client directory to build the React Client
* run `npm run server` within the project directory to start the Express server at port 3000 with will serve the built static files
