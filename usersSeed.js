"use strict"

const mongoose = require("mongoose"),
  User = require("./models/user"),
  Course = require("./models/course"),
  StudyBook = require("./models/studyBook"),
  Question = require("./models/question"),
  { createCourses, getTestCourses } = require("./coursesSeed");

/*
 * Synchronously map of course title to course id.
 * titles: array of course titles.
 * return: map of course title to course id.
 */
const getCourseIdsByTitles = async(titles) => {
  let commands = [];
  let courseTitleToId = {};

  titles.forEach((title) => {
    commands.push(Course.findOne({title: title})
      .then(course => {
        courseTitleToId[title] = course._id;
      }));
  });

  await Promise.all(commands);

  return courseTitleToId;
}

/*
 * Replace course titles with course ids in users.
 * users: array of user objects with courses stored as titles.
 * courseTitleToId: map of course titles to course id.
 * return: array of user objects with courses stored as ids.
 */
const insertCourseIds = (users, courseTitleToId) => {
  return users.map(user => {
    return {
      ...user,
      enrolledCourses: user.enrolledCourses.map(course => {
        return courseTitleToId[course];
      })
    }
  });
}

/*
 * Creates users from a list of JSON representations with enrolledCourses represented as course ids.
 * Synchronously creates users.
 * These steps are done synchronously because they are required to be completed before
 * moving on.
 * Returns an array of the newly created user ids.
 * The calling method can use the user ids because the users are guaranteed
 * to have been created.
 * course: array of user JSON objects.
 * return: array of users ids.
 */
const createUsers = async(users) => {
  let commands = [];
  let userIds = [];

  users.forEach((u) => {
    commands.push(User.register(
      {
        name: u.name,
        username: u.username,
        email: u.email,
        enrolledCourses: u.enrolledCourses
      },
      u.username
      )
      .then(userDoc => {
        console.log("User:\n" + userDoc);
        userIds.push(userDoc._id);
      })
    );
  });

  await Promise.all(commands);

  return userIds;
}

/*
 * Get array of test user objects.
 */
const getTestUsers = () => {
  return [
    {
      name: {
        first: "Tony",
        last: "Stark"
      },
      username: "ironman",
      email: "tony_stark@starkindustries.com",
      enrolledCourses: ["CSC 111", "CSC 380"]
    },
    {
      name: {
        first: "SpongeBob",
        last: "SquarePants"
      },
      username: "krabbypatty",
      email: "frycook@krustykrab.com",
      enrolledCourses: ["CSC 112"]
    },
    {
      name: {
        first: "Darth",
        last: "Vader"
      },
      username: "darkside",
      email: "lordvader@empire.com",
      enrolledCourses: ["CSC 380"]
    }
  ];
}

module.exports = {
  getCourseIdsByTitles: getCourseIdsByTitles,
  insertCourseIds: insertCourseIds,
  createUsers: createUsers,
  getTestUsers: getTestUsers
}
