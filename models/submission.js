"use strict"

const mongoose = require("mongoose"),
  { Schema } = require("mongoose");

var submissionSchema = new Schema({
    response: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      required: true,
      min: [0, "Course cannot have a negative rating"],
      max: [5, "Maximum rating is 5"]
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    question: {
      type: Schema.Types.ObjectId,
      ref: "Question",
      required: true
    }
  },
  {
    timestamps: true
  }
);

submissionSchema.index({
    user: 1,
    question: 1
  },
  {
    unique: true
  }
);

module.exports = mongoose.model("Submission", submissionSchema);
