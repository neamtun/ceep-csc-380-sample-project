"use strict"

const mongoose = require("mongoose"),
  { Schema } = require("mongoose");

var questionSchema = new Schema({
    text: {
      type: String,
      required: true,
      unique: true
    },
    answer: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Question", questionSchema);
