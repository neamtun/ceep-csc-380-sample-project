"use strict"

const mongoose = require("mongoose"),
  { Schema } = require("mongoose");

var courseSchema = new Schema({
    title: {
      type: String,
      required: true,
      unique: true
    },
    description: {
      type: String,
      required: true
    },
    maxStudents: {
      type: Number,
      default: 0,
      min: [0, "Course cannot have a negative number of students"]
    },
    studyBooks: [
      {
        type: Schema.Types.ObjectId,
        ref: "StudyBook"
      }
    ]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Course", courseSchema);
