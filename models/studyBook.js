"use strict"

const mongoose = require("mongoose"),
  { Schema } = require("mongoose");

var studyBookSchema = new Schema({
    title: {
      type: String,
      required: true,
      unique: true
    },
    questions: [
      {
        type: Schema.Types.ObjectId,
        ref: "Question"
      }
    ]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("StudyBook", studyBookSchema);
