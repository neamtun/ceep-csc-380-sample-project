"use strict"

const router = require("express").Router(),
  studyBooksController = require("../controllers/studyBooksController"),
  usersController = require("../controllers/usersController"),
  homeController = require("../controllers/homeController");

router.get("/", usersController.loggedIn, studyBooksController.all, homeController.redirect, studyBooksController.indexView);
router.get("/:studyBookId", usersController.loggedIn, studyBooksController.show, homeController.redirect, studyBooksController.showView);

module.exports = router;
