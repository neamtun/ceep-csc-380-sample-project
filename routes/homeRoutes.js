"use strict"

const router = require("express").Router(),
  homeController = require("../controllers/homeController"),
  usersController = require("../controllers/usersController");

router.get("/", usersController.loggedIn, homeController.redirect, homeController.index);

module.exports = router;
