"use strict"

const router = require("express").Router(),
  submissionsController = require("../controllers/submissionsController"),
  usersController = require("../controllers/usersController"),
  homeController = require("../controllers/homeController");

router.get("/", usersController.loggedIn, submissionsController.all, homeController.redirect, submissionsController.indexView);
router.get("/:submissionId", usersController.loggedIn, submissionsController.show, homeController.redirect, submissionsController.showView);

module.exports = router;
