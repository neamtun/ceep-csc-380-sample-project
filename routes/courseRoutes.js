"use strict"

const router = require("express").Router(),
  coursesController = require("../controllers/coursesController"),
  usersController = require("../controllers/usersController"),
  homeController = require("../controllers/homeController");

router.get("/", usersController.loggedIn, coursesController.all, homeController.redirect, coursesController.indexView);
router.get("/:courseId", usersController.loggedIn, coursesController.show, homeController.redirect, coursesController.showView);

module.exports = router;
