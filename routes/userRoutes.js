"use strict"

const router = require("express").Router(),
  usersController = require("../controllers/usersController"),
  homeController = require("../controllers/homeController"),
  coursesController = require("../controllers/coursesController"),
  studyBooksController = require("../controllers/studyBooksController"),
  questionsController = require("../controllers/questionsController"),
  submissionsController = require("../controllers/submissionsController");

router.get("/", usersController.loggedIn, usersController.all, homeController.redirect, usersController.indexView);
router.get("/new", coursesController.all, usersController.new);
router.post("/create", usersController.validate, usersController.create, homeController.redirect, usersController.authenticate);
router.get("/login", usersController.login);
router.post("/login", usersController.authenticate);
router.get("/logout", usersController.logout, homeController.redirect);
router.get("/:userId/edit", usersController.loggedIn, homeController.redirect, coursesController.all, usersController.edit);
router.put("/:userId/update", usersController.loggedIn, usersController.update, homeController.redirect);
router.get("/:userId", usersController.loggedIn, usersController.show, homeController.redirect, usersController.showView);
router.delete("/:userId/delete", usersController.loggedIn, usersController.delete, submissionsController.deleteByUserId, homeController.redirect);
router.get("/:userId/enrolled-courses", usersController.loggedIn, usersController.indexEnrolledCourses, homeController.redirect, usersController.indexEnrolledCoursesView);
router.get("/:userId/enrolled-courses/:courseId", usersController.loggedIn, usersController.validateEnrolled, coursesController.show, homeController.redirect,
  coursesController.showEnrolledCoursesView)
router.get("/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/previous-submission", usersController.loggedIn, usersController.validateEnrolled,
  coursesController.validateActiveStudyBook,
  submissionsController.previousSubmission,
  homeController.redirect,
  submissionsController.previousSubmissionView);
router.get("/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/active-question/:questionId", usersController.loggedIn, usersController.validateEnrolled,
  coursesController.validateActiveStudyBook,
  studyBooksController.validateActiveQuestion,
  homeController.redirect,
  questionsController.activeQuestionView);
router.put("/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/active-question/:questionId", usersController.loggedIn, usersController.validateEnrolled,
  coursesController.validateActiveStudyBook,
  studyBooksController.validateActiveQuestion,
  submissionsController.submitActiveResponse,
  studyBooksController.nextActiveQuestion,
  homeController.redirect,
  questionsController.activeQuestionView);

module.exports = router;
