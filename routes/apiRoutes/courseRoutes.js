"use strict"

const router = require("express").Router(),
  coursesController = require("../../controllers/coursesController"),
  usersController = require("../../controllers/usersController"),
  homeController = require("../../controllers/homeController");


router.get("/", usersController.loggedInAPI, coursesController.all, homeController.redirect, homeController.respondJSON);
router.get("/:courseId", usersController.loggedInAPI, coursesController.show, homeController.redirect, homeController.respondJSON);

module.exports = router;
