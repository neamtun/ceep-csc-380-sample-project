"use strict"

const router = require("express").Router(),
  submissionsController = require("../../controllers/submissionsController"),
  usersController = require("../../controllers/usersController"),
  homeController = require("../../controllers/homeController");

router.get("/", usersController.loggedInAPI, submissionsController.all, homeController.redirect, homeController.respondJSON);
router.get("/:submissionId", usersController.loggedInAPI, submissionsController.show, homeController.redirect, homeController.respondJSON);

module.exports = router;
