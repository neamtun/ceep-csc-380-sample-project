"use strict"

const router = require("express").Router(),
  homeController = require("../../controllers/homeController"),
  usersController = require("../../controllers/usersController");

router.get("/", usersController.loggedInAPI, homeController.redirect, homeController.respondJSON);

module.exports = router;
