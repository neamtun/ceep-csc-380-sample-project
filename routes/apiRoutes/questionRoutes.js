"use strict"

const router = require("express").Router(),
  questionsController = require("../../controllers/questionsController"),
  usersController = require("../../controllers/usersController"),
  homeController = require("../../controllers/homeController");

router.get("/", usersController.loggedInAPI, questionsController.all, homeController.redirect, homeController.respondJSON);
router.get("/:questionId", usersController.loggedInAPI, questionsController.show, homeController.redirect, homeController.respondJSON);

module.exports = router;
