"use strict"

const router = require("express").Router(),
  studyBooksController = require("../../controllers/studyBooksController"),
  usersController = require("../../controllers/usersController"),
  homeController = require("../../controllers/homeController");

router.get("/", usersController.loggedInAPI, studyBooksController.all, homeController.redirect, homeController.respondJSON);
router.get("/:studyBookId", usersController.loggedInAPI, studyBooksController.show, homeController.redirect, homeController.respondJSON);

module.exports = router;
