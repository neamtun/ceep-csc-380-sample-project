"use strict"

const router = require("express").Router(),
  homeRoutes = require("./homeRoutes"),
  userRoutes = require("./userRoutes"),
  courseRoutes = require("./courseRoutes"),
  studyBookRoutes = require("./studyBookRoutes"),
  questionRoutes = require("./questionRoutes"),
  submissionRoutes = require("./submissionRoutes");

router.use("/users", userRoutes);
router.use("/courses", courseRoutes);
router.use("/study-books", studyBookRoutes);
router.use("/questions", questionRoutes);
router.use("/submissions", submissionRoutes);
router.use("/", homeRoutes);

module.exports = router;
