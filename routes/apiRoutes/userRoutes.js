"use strict"

const router = require("express").Router(),
  usersController = require("../../controllers/usersController"),
  homeController = require("../../controllers/homeController"),
  coursesController = require("../../controllers/coursesController"),
  studyBooksController = require("../../controllers/studyBooksController"),
  questionsController = require("../../controllers/questionsController"),
  submissionsController = require("../../controllers/submissionsController");

router.get("/", usersController.loggedInAPI, usersController.all, homeController.redirect, homeController.respondJSON);
router.get("/new", coursesController.all, homeController.respondJSON);
router.post("/create", usersController.validateAPI, usersController.createAPI, homeController.redirect, usersController.authenticateAPI);
router.get("/login", homeController.respondJSON);
router.post("/login", usersController.authenticateAPI);
router.get("/logout", usersController.logoutAPI, homeController.redirect, homeController.respondJSON);
router.get("/:userId/edit", usersController.loggedInAPI, homeController.redirect, coursesController.all, usersController.editAPI, homeController.respondJSON);
router.put("/:userId/update", usersController.loggedInAPI, usersController.updateAPI, homeController.redirectAsGet);
router.get("/:userId", usersController.loggedInAPI, usersController.show, homeController.redirect, homeController.respondJSON);
router.delete("/:userId/delete", usersController.loggedInAPI, usersController.deleteAPI, submissionsController.deleteByUserId, homeController.redirectAsGet);
router.get("/:userId/enrolled-courses", usersController.loggedInAPI, usersController.indexEnrolledCourses, homeController.redirect,
 homeController.respondJSON);
router.get("/:userId/enrolled-courses/:courseId", usersController.loggedInAPI, usersController.validateEnrolledAPI, coursesController.show,
  homeController.redirect,
  homeController.respondJSON)
router.get("/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/previous-submission", usersController.loggedInAPI, usersController.validateEnrolledAPI,
  coursesController.validateActiveStudyBookAPI,
  submissionsController.previousSubmission,
  homeController.redirect,
  homeController.respondJSON);
router.get("/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/active-question/:questionId", usersController.loggedInAPI, usersController.validateEnrolledAPI,
  coursesController.validateActiveStudyBookAPI,
  studyBooksController.validateActiveQuestionAPI,
  homeController.redirect,
  homeController.respondJSON);
router.put("/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/active-question/:questionId", usersController.loggedIn, usersController.validateEnrolledAPI,
  coursesController.validateActiveStudyBookAPI,
  studyBooksController.validateActiveQuestionAPI,
  submissionsController.submitActiveResponse,
  studyBooksController.nextActiveQuestionAPI,
  homeController.redirectAsGet,
  homeController.respondJSON);

module.exports = router;
