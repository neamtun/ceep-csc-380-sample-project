"use strict"

const router = require("express").Router(),
  questionsController = require("../controllers/questionsController"),
  usersController = require("../controllers/usersController"),
  homeController = require("../controllers/homeController");

router.get("/", usersController.loggedIn, questionsController.all, homeController.redirect, questionsController.indexView);
router.get("/:questionId", usersController.loggedIn, questionsController.show, homeController.redirect, questionsController.showView);

module.exports = router;
