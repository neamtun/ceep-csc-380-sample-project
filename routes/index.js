"use strict"

const router = require("express").Router(),
  apiRoutes = require("./apiRoutes/index");

router.use("/api", apiRoutes);

module.exports = router;
