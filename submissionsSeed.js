"use strict"

const mongoose = require("mongoose"),
  Submission = require("./models/submission"),
  User = require("./models/user"),
  Question = require("./models/question"),
  Course = require("./models/course"),
  StudyBook = require("./models/studyBook");

/*
 * Gets current users and populates inner references.
 * Done asynchronously to guarantee the operations complete before the next commands.
 * return: array of users with populated references.
 */
const populateCurrentUsers = async() => {
  return await User.find()
    .populate({
      path: "enrolledCourses",
      populate: {
        path: "studyBooks",
        populate: {
          path: "questions"
        }
      }
    });
}

/*
 * Create submission for each question every user can answer.
 * Done synchronously to ensure the database operations are finished before the next commands.
 * Returns submission ids and guarantees the submissions exist for the caller.
 * users: array of users with inner references populated.
 * return: array of newly created submission ids.
 */
const createSubmissions = async(users) => {
  let commands = [];
  let submissionIds = [];

  users.forEach(user => {
    user.enrolledCourses.forEach(course => {
      course.studyBooks.forEach(studyBook => {
        studyBook.questions.forEach(question => {
          commands.push(Submission.create(
            {
              response: question.answer,
              rating: Math.floor(Math.random() * 6),
              user: user._id,
              question: question._id
            }
            )
            .then(submissionDov => {
              console.log("Submission:\n" + submissionDov);
              submissionIds.push(submissionDov._id);
            })
          );
        });
      });
    });
  });

  await Promise.all(commands);

  return submissionIds;
}

module.exports = {
  populateCurrentUsers: populateCurrentUsers,
  createSubmissions: createSubmissions
}
