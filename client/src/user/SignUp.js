import React from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

import CourseCheckBoxes from "../components/CourseCheckBoxes";
import { useAuth } from "../context/auth";
import usePublicGetRequest from "../hooks/usePublicGetRequest";
import { getCheckedCourseIds, onInputChange, handleCheck, mapNewResponse } from "./util";

const SignUp = () => {
  const { setUser } = useAuth();

  const NEW_ENDPOINT = "/api/users/new";
  const RESULT_FIELD = "";
  const CREATE_ENDPOINT = "/api/users/create/";

  const [response, setResponse] = usePublicGetRequest(NEW_ENDPOINT, RESULT_FIELD, mapNewResponse);

  const handleCreateUser = (event) => {
    event.preventDefault();

    const firstName = response.data.user.firstName,
      lastName = response.data.user.lastName,
      email = response.data.user.email,
      username = response.data.user.username,
      password = response.data.user.password,
      enrolledCourses = getCheckedCourseIds(response.data.courses);

    axios.post(CREATE_ENDPOINT, {
        firstName,
        lastName,
        email,
        enrolledCourses,
        username,
        password
      })
      .then(result => {
        if (result.status === 200 && result.data.status === 200 && result.data.data.loggedIn) {
          const currentUser = { loggedIn: true, ...result.data.data.currentUser };

          setUser(currentUser);
          setResponse({ ...response, loggedIn: true, errorMessage: "" });
        }
        else {
          // Create user failed.
          if (result.data.data.flashMessages.error) {
            setResponse({ ...response,
              loggedIn: false,
              errorMessage: result.data.data.flashMessages.error.join(", ") });
          }
          else {
            setResponse({ ...response,
              loggedIn: false,
              errorMessage: "Something went wrong..." });
          }
        }
      })
      .catch(() =>
        // Unknown error.
        setResponse({ ...response,
          loggedIn: false,
          errorMessage: "Something went wrong..." })
      );
  };

  if (response.loggedIn) {
    return <Redirect to="/"/>
  }

  return (
    <div className="d-flex justify-content-center align-items-center text-center h-100">
      <form className="form" onSubmit={handleCreateUser}>
        <h3 className="h3 font-weight-normal mb-3">Sign Up</h3>

        <Form.Control id="firstName" placeholder="First Name" value={Object.keys(response.data).length > 0 ? response.data.user.firstName : ""} type="text" onChange={onInputChange("firstName", response, setResponse)} required autoFocus={true}/>
        <Form.Control id="lastName" placeholder="Last Name" value={Object.keys(response.data).length > 0 ? response.data.user.lastName : ""} type="text" onChange={onInputChange("lastName", response, setResponse)} required/>
        <Form.Control id="email" placeholder="Email" value={Object.keys(response.data).length > 0 ? response.data.user.email : ""} type="text" onChange={onInputChange("email", response, setResponse)} required/>

        <CourseCheckBoxes courses={Object.keys(response.data).length > 0 ? response.data.courses : []} onCheck={handleCheck(response, setResponse)}>
          Courses:
        </CourseCheckBoxes>

        <Form.Control id="username" placeholder="Username" value={Object.keys(response.data).length > 0 ? response.data.user.username : ""} type="text" onChange={onInputChange("username", response, setResponse)} required/>
        <Form.Control id="password" placeholder="Password" value={Object.keys(response.data).length > 0 ? response.data.user.password : ""} type="password" onChange={onInputChange("password", response, setResponse)} required/>

        <div className="mt-3 mb-2">
          <Button variant="outline-primary" type="submit">Submit</Button>
        </div>

        <Form.Text><Link to="/users/login">Login</Link> with an existing username.</Form.Text>
        <Form.Text className="text-danger">{response.errorMessage}</Form.Text>
      </form>
    </div>
  );
};

export default SignUp;
