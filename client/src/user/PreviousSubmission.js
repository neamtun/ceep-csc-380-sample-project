import React from "react";
import { useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";

import { ratingClass } from "../Submission/util";

const tableHeaders = [
  { header: "Response", key: "response" },
  { header: "Correct Answer", key: "correctAnswer" },
  { header: "Question", key: "question" },
  { header: "Rating", key: "rating" },
  { header: "Submitted At", key: "submittedAt" },
];

const mapSubmissions = submissions => {
  return submissions.map(submission => {
    return {
      ...submission,
      correctAnswer: submission.question.answer,
      question: submission.question.text,
      submittedAt: new Date(submission.updatedAt).toLocaleString(),
      rating: <div className={ratingClass(submission.rating)}>{`${submission.rating}`}</div>
    };
  });
};

const mapResponse = response => {
  return {
    ...response,
    submissions: mapSubmissions(response.submissions)
  };
};

const PreviousSubmission = () => {
  const { userId, courseId, studyBookId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/users/${userId}/enrolled-courses/${courseId}/active-study-book/${studyBookId}/previous-submission`;
  const RESULT_FIELD = "";

  const response = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapResponse);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">{Object.keys(response.data).length > 0 ? `${response.data.course.title}: ${response.data.course.description}` : "..."}</h3>
      <h4 className="h4 font-weight-normal">{Object.keys(response.data).length > 0 ? `${response.data.studyBook.title}` : "..."}</h4>

      {response.isLoading ?
        <Spinner animation="border" role="status"/> :
        response.errorMessage ?
          <p className="text-danger">{response.errorMessage}</p> :
          <Table headers={tableHeaders} rows={Object.keys(response.data).length > 0 ? response.data.submissions : []} responsive="lg"/>
      }
    </div>
  );
};

export default PreviousSubmission;
