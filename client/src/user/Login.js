import React from "react";
import { Link, Redirect, useLocation } from "react-router-dom";
import axios from "axios";

import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

import { useAuth } from "../context/auth"

const Login = () => {
  const { user, setUser } = useAuth();
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [loginStatus, setLoginStatus] = React.useState({ loggedIn: user.loggedIn, errorMessage: "" });
  const location = useLocation();
  const referrer = location.state ? location.state.referrer : "/";

  const API_ENDPOINT = "/api/users/login";

  const onInputChange = (setFunc) => {
    return (event) =>
      setFunc(event.target.value);
  };

  const handleLogin = (event) => {
    event.preventDefault();

    axios.post(API_ENDPOINT, { username, password })
      .then(result => {
        if (result.status === 200 && result.data.status === 200 && result.data.data.loggedIn) {
          const currentUser = { loggedIn: true, ...result.data.data.currentUser };

          setUser(currentUser);
          setLoginStatus({ ...loginStatus, loggedIn: true, errorMessage: "" });
        }
        else {
          // Login failed.
          if (result.data.data.flashMessages.error) {
            setLoginStatus({ ...loginStatus,
              loggedIn: false,
              errorMessage: result.data.data.flashMessages.error.join(", ") });
          }
          else {
            setLoginStatus({ ...loginStatus,
              loggedIn: false,
              errorMessage: "Something went wrong..." });
          }
        }
      })
      .catch(() =>
        // Unknown error.
        setLoginStatus({ ...loginStatus,
          loggedIn: false,
          errorMessage: "Something went wrong..." })
      );
  };

  if (loginStatus.loggedIn) {
    return <Redirect to={referrer}/>
  }

  return (
    <div className="d-flex justify-content-center align-items-center text-center h-100">
      <form className="form" onSubmit={handleLogin}>
        <h3 className="h3 font-weight-normal mb-3">Login</h3>

        <div>
          <Form.Control id="username" placeholder="Username" value={username} type="text" onChange={onInputChange(setUsername)} required autoFocus={true}/>
          <Form.Control id="password" placeholder="Password" value={password} type="password" onChange={onInputChange(setPassword)} required/>
        </div>

        <div className="mt-3 mb-2">
          <Button variant="outline-primary" type="submit">Login</Button>
        </div>

        <Form.Text>Need an account? Sign up <Link to="/users/new">here</Link>.</Form.Text>
        <Form.Text className="text-danger">{loginStatus.errorMessage}</Form.Text>
      </form>
    </div>
  );
};

export default Login;
