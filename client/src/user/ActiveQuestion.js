import React from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequestWithUpdate from "../hooks/usePrivateGetRequestWithUpdate";

const ReferrerRedirectAndClearResponse = ({ to, setResponse} ) => {
  setResponse(mapResponse( { data: {} } ));
  return <ReferrerRedirect to={to}/>
}

const mapResponse = data => {
  return {
    ...data,
    response: "",
    rating: 0,
    redirect: ""
  };
};

const onInputChange = (field, response, setResponse) => {
  return (event) => {
    const data = response.data;
    data[field] = event.target.value;
    setResponse({ ...response, data: data})
  };
};

const ActiveQuestion = () => {
  const { userId, courseId, studyBookId, questionId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const BASE_URL = `/users/${userId}/enrolled-courses/${courseId}/active-study-book/${studyBookId}/`;
  const GET_URL = `${BASE_URL}active-question/`;
  const GET_ENDPOINT = `/api${GET_URL}`;
  const RESULT_FIELD = "";
  const PUT_ENDPOINT = `${GET_ENDPOINT}${questionId}`;

  const [response, setResponse] = usePrivateGetRequestWithUpdate(setLoggedIn, `${GET_ENDPOINT}${questionId}`, RESULT_FIELD, mapResponse);

  const submitResponse = event => {
    event.preventDefault();

    axios.put(PUT_ENDPOINT, {
      response: response.data.response,
      rating: response.data.rating
      })
      .then(result => {
        if (result.status === 200 && result.data.status === 200) {
          if (result.data.data.question) {
            setResponse({ ...response, redirect: `${GET_URL}${result.data.data.question._id}`, errorMessage: "" });
          }
          else {
            setResponse({ ...response, redirect: `${BASE_URL}previous-submission`, errorMessage: "" });
          }
        }
        else {
          if (result.data.data.flashMessages.error) {
            setResponse({ ...response, redirect: `GET_ENDPOINT`, errorMessage: result.data.data.flashMessages.error.join(", ") });
          }
          else {
            setResponse({ ...response, redirect: "", errorMessage: "Something went wrong..." });
          }
        }
      })
      .catch(() => setResponse({ ...response, redirect: "", errorMessage: "Something went wrong..." }));
  };

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center text-center">
      {response.redirect ?
        <ReferrerRedirectAndClearResponse to={response.redirect} setResponse={setResponse}/> :
        <>
          <form onSubmit={submitResponse}>
            <h3 className="h3 font-weight-normal pt-3">
              {Object.keys(response.data).length > 0 ? `${response.data.course.title}: ${response.data.course.description}` : "..."}
            </h3>
            <h4 className="h4 font-weight-normal">
              {Object.keys(response.data).length > 0 ? `${response.data.studyBook.title}` : "..."}
            </h4>

            {Object.keys(response.data).length > 0 ?
              <Form.Group>
                <Form.Label>{response.data.question.text}</Form.Label>
                <Form.Control as="textarea" id="response" value={response.data.response} type="textarea" rows="8" cols="80" onChange={onInputChange("response", response, setResponse)}/>
              </Form.Group> :
              <Spinner animation="border" role="status"/>
            }

            {Object.keys(response.data).length > 0 ?
              <Form.Group className="d-flex flex-column align-items-center">
                <Form.Label>Give a confidence rating for your response (0-5):</Form.Label>
                <Form.Control id="rating" value={response.data.rating} type="number" min="0" max="5" onChange={onInputChange("rating", response, setResponse)}/>
              </Form.Group>:
              ""
            }

            <Button variant="outline-primary" type="submit">Submit</Button>
          </form>
        </>
      }
  </div>
  );
};

export default ActiveQuestion;
