import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";

import PrivateRoute from "../components/PrivateRoute";

import ActiveQuestion from "./ActiveQuestion";
import Edit from "./Edit";
import EnrolledCourse from "./EnrolledCourse"
import EnrolledCourses from "./EnrolledCourses";
import Login from "./Login";
import Logout from "./Logout";
import PreviousSubmission from "./PreviousSubmission";
import SignUp from "./SignUp";
import User from "./User";
import Users from "./Users";

const UserRoutes = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute exact={true} path={path}>
        <Users/>
      </PrivateRoute>
      <Route path={`${path}/login`}>
        <Login/>
      </Route>
      <Route path={`${path}/logout`}>
        <Logout/>
      </Route>
      <Route path={`${path}/new`}>
        <SignUp/>
      </Route>
      <PrivateRoute path={`${path}/:userId/edit`}>
        <Edit/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/previous-submission`}>
        <PreviousSubmission/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:userId/enrolled-courses/:courseId/active-study-book/:studyBookId/active-question/:questionId`}>
        <ActiveQuestion/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:userId/enrolled-courses/:courseId`}>
        <EnrolledCourse/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:userId/enrolled-courses`}>
        <EnrolledCourses/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:userId`}>
        <User/>
      </PrivateRoute>
    </Switch>
  );
};

export default UserRoutes;
