import React from "react";
import { useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { mapActiveUser } from "./util";

const tableHeaders = [
  { header: "Title", key: "title" },
  { header: "Description", key: "description" }
];

const EnrolledCourses = () => {
  const { userId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/users/${userId}/enrolled-courses`;
  const RESULT_FIELD = "user";

  const response = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapActiveUser(userId));

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Enrolled Courses for {Object.keys(response.data).length > 0 ?
        `${response.data.firstname} ${response.data.lastname}` :
        "..."}
      </h3>

      {response.isLoading ?
        <Spinner animation="border" role="status"/> :
        response.errorMessage ?
          <p className="text-danger">{response.errorMessage}</p> :
          <Table headers={tableHeaders} rows={Object.keys(response.data).length > 0 ? response.data.enrolledCourses : []} responsive="md"/>
      }
    </div>
  );
};

export default EnrolledCourses;
