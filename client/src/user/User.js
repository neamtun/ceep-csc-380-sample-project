import React from "react";
import { Link, Redirect, useParams } from "react-router-dom";
import axios from "axios";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapUser } from "./util";

const User = () => {
  const { userId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();
  const [deleteStatus, setDeleteStatus] = React.useState({ redirect: false, errorMessage: "" });

  const USER_ENDPOINT = `/api/users/${userId}`;
  const RESULT_FIELD = "user";

  const user = usePrivateGetRequest(setLoggedIn, USER_ENDPOINT, RESULT_FIELD, mapUser);

  const DELETE_ENDPOINT = `/api/users/${userId}/delete`;

  const deleteUser = () => {
    if (window.confirm("Are you sure you would like to delete this user?")) {
      axios.delete(DELETE_ENDPOINT)
        .then(result => {

          if (result.status === 200 && result.data.status === 200) {
            if (result.data.loggedIn === false) {
              setLoggedIn(false);
              setDeleteStatus({ ...deleteUser, redirect: false, errorMessage: "" });
            }
            else {
              setDeleteStatus({ ...deleteUser, redirect: true, errorMessage: "" });
            }
          }
          else {
            setDeleteStatus({ ...deleteUser, redirect: false, errorMessage: "Something went wrong. Unable to delete user..." });
          }
        })
        .catch(() => setDeleteStatus({ ...deleteUser, redirect: false, errorMessage: "Something went wrong. Unable to delete user..." }));
    }
  };

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }
  else if (deleteStatus.redirect) {
    return <Redirect to="/users"/>
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">
        User Profile for {Object.keys(user.data).length > 0 ?
          `${user.data.firstname} ${user.data.lastname}` :
          "..."}
      </h3>

      {user.isLoading ?
        <Spinner animation="border" role="status"/> :
        user.errorMessage ?
          <p className="text-danger">{user.errorMessage}</p> : (
          <>
            <Table headers={tableHeaders} rows={Object.keys(user.data).length > 0 ? [user.data] : []} responsive="lg"/>
            <Link to={`/users/${user.data._id}/edit`}>Edit</Link>
            <Link to={`/users/${user.data._id}`} onClick={deleteUser}>Delete</Link>
          </>
        )}

      {deleteStatus.errorMessage && <p className="text-danger">deleteStatus.errorMessage</p>}
    </div>
  )
};

export default User;
