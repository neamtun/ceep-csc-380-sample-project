import React from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";

import useEffectOnlyOnce from "../hooks/useEffectOnlyOnce";

import { useAuth } from "../context/auth";

const Logout = () => {
  const { setUser } = useAuth();

  const API_ENDPOINT = "/api/users/logout";

  const logout = () => {
    axios.get(API_ENDPOINT)
      .then(result => {
        setUser( {} );
      });
  };

  useEffectOnlyOnce(logout);

  return (
    <Redirect to="/users/login"/>
  )
}

export default Logout;
