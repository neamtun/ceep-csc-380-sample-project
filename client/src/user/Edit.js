import React from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

import CourseCheckBoxes from "../components/CourseCheckBoxes";
import ReferrerRedirect from "../components/ReferrerRedirect";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequestWithSetter from "../hooks/usePrivateGetRequestWithSetter";
import { getCheckedCourseIds, onInputChange, handleCheck, mapEditResponse } from "./util";

const Edit = () => {
  const { userId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const EDIT_ENDPOINT = `/api/users/${userId}/edit`;
  const RESULT_FIELD = "";
  const UPDATE_ENDPOINT = `/api/users/${userId}/update`

  const [response, setResponse] = usePrivateGetRequestWithSetter(setLoggedIn, EDIT_ENDPOINT, RESULT_FIELD, mapEditResponse);

  const handleEditUser = (event) => {
    event.preventDefault();

    const firstName = response.data.user.firstName,
      lastName = response.data.user.lastName,
      email = response.data.user.email,
      username = response.data.user.username,
      enrolledCourses = getCheckedCourseIds(response.data.courses);

    axios.put(UPDATE_ENDPOINT, {
      firstName,
      lastName,
      email,
      username,
      enrolledCourses
    })
    .then(result => {
      if (result.status === 200 && result.data.status === 200) {
        setResponse({ ...response, updated: true, errorMessage: "" });
      }
      else {
        if (result.data.data.flashMessages.error) {
          setResponse({ ...response, updated: false, errorMessage: result.data.data.flashMessages.error.join(", ") });
        }
        else {
          setResponse({ ...response, updated: false, errorMessage: "Something went wrong..." });
        }
      }
    })
    .catch(() => setResponse({ ...response, updated: false, errorMessage: "Something went wrong..." }));
  }

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }
  else if (response.updated) {
    return <ReferrerRedirect to={`/users/${userId}`}/>
  }

  return (
    <div className="d-flex justify-content-center align-items-center text-center h-100">
      <form className="form" onSubmit={handleEditUser}>
        <h3 className="h3 font-weight-normal mb-3">Edit Profile for {Object.keys(response.data).length > 0 ? `${response.data.user.firstName} ${response.data.user.lastName}` : "..."}</h3>

        <Form.Control id="firstName" placeholder="First Name" value={Object.keys(response.data).length > 0 ? response.data.user.firstName : ""} type="text" onChange={onInputChange("firstName", response, setResponse)} autoFocus={true} required/>
        <Form.Control id="lastName" placeholder="Last Name" value={Object.keys(response.data).length > 0 ? response.data.user.lastName : ""} type="text" onChange={onInputChange("lastName", response, setResponse)} required/>
        <Form.Control id="email" placeholder="Email" value={Object.keys(response.data).length > 0 ? response.data.user.email : ""} type="text" onChange={onInputChange("email", response, setResponse)} required/>
        <Form.Control id="username" placeholder="Username" value={Object.keys(response.data).length > 0 ? response.data.user.username : ""} type="text" onChange={onInputChange("username", response, setResponse)} required/>

        <CourseCheckBoxes courses={Object.keys(response.data).length > 0 ? response.data.courses : []} onCheck={handleCheck(response, setResponse)}>
          Courses:
        </CourseCheckBoxes>

        <div className="mt-3 mb-2">
          <Button variant="outline-primary" type="submit">Submit</Button>
        </div>

        <Form.Text className="text-danger">{response.errorMessage}</Form.Text>
      </form>
    </div>
  )
}

export default Edit;
