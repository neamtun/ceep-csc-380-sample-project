import React from "react";
import { Link, useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";

import { sortStudyBooks } from "../StudyBook/util";

const tableHeaders = [
  { header: "Study Book", key: "title" },
  { header: "Take Quiz", key: "takeQuiz" },
  { header: "Previous Submission", key: "previousSubmission" }
]

const mapStudyBooks = (userId, courseId, studyBooks) => {
  return studyBooks.sort(sortStudyBooks).map(studyBook => {
    return {
      ...studyBook,
      takeQuiz: <Link to={`/users/${userId}/enrolled-courses/${courseId}/active-study-book/${studyBook._id}/active-question/${studyBook.questions[0]}`}>Quiz</Link>,
      previousSubmission: <Link to={`/users/${userId}/enrolled-courses/${courseId}/active-study-book/${studyBook._id}/previous-submission`}>Submission</Link>
    }
  });
};

const mapCourse = (userId, courseId) => {
  return (course) => {
    return {
      ...course,
      studyBooks: mapStudyBooks(userId, courseId, course.studyBooks)
    };
  };
};


const EnrolledCourse = () => {
  const { userId, courseId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/users/${userId}/enrolled-courses/${courseId}`;
  const RESULT_FIELD = "course";

  const response = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapCourse(userId, courseId));

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">
        {Object.keys(response.data).length > 0 ?
          `${response.data.title}: ${response.data.description}` :
          "..."}
      </h3>

      {response.isLoading ?
        <Spinner animation="border" role="status"/> :
        response.errorMessage ?
          <p className="text-danger">{response.errorMessage}</p> :
          <Table headers={tableHeaders} rows={Object.keys(response.data).length > 0 ? response.data.studyBooks : []} responsive="lg"/>
      }
    </div>
  );
};

export default EnrolledCourse;
