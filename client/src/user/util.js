import React from "react";
import { Link } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import { sortCourses } from "../Course/util";

const tableHeaders = [
  { header: "Username", key: "username"},
  { header: "First Name", key: "firstname"},
  { header: "Last Name", key: "lastname"},
  { header: "Email", key: "email"},
  { header: "Enrolled Courses", key: "enrolledCourses"},
  { header: "Created At", key: "createdAt"},
  { header: "Updated At", key: "updatedAt"}
];

const mapEnrolledCoursesStrings = courses => {
  return <Container>{courses.sort(sortCourses).map(course => <Row key={course._id}>{course.title}</Row>)}</Container>;
};

const mapEnrolledCoursesLinks = courses => {
  return <Container>{courses.sort(sortCourses).map(course => <Row key={course._id}><Link to={`/courses/${course._id}`}>{course.title}</Link></Row>)}</Container>;
};

const mapUsernameString = user => user.username;

const mapUsernameLink = user => <Link to={`/users/${user._id}`}>{user.username}</Link>;

const mapUserHOF = (mapUsername, mapEnrolledCourses) => {
  return ({ name, ...user}) => {
    return {
      ...user,
      firstname: name.first,
      lastname: name.last,
      username: mapUsername(user),
      enrolledCourses: mapEnrolledCourses(user.enrolledCourses),
      createdAt: new Date(user.createdAt).toLocaleString(),
      updatedAt: new Date(user.updatedAt).toLocaleString()
    };
  };
};

const sortUsers = (user1, user2) => {
  if (user1.username > user2.username) {
    return 1;
  }
  else if (user1.username < user2.username) {
    return -1;
  }
  else {
    return 0;
  }
};

const mapUser = mapUserHOF(mapUsernameString, mapEnrolledCoursesStrings);

const mapUsers = users => users.sort(sortUsers).map(mapUserHOF(mapUsernameLink, mapEnrolledCoursesLinks));

const getCheckedCourseIds = courses => {
  return courses.data.filter(course => courses.checked.includes(course.title)).map(course => course._id);
};

const mapCourseObjects = courses => {
  return courses.sort(sortCourses).map(course => {
    return { title: course.title, _id: course._id };
  });
};

const onInputChange = (field, response, setResponse) => {
  return (event) => {
    const data = response.data;
    const user = response.data.user;
    user[field] = event.target.value;
    setResponse({ ...response, data: { ...data, user: user } })
  };
};

const handleCheck = (response, setResponse) => {
  return (event) => {
    const value = event.target.value;
    const checked = response.data.courses.checked.includes(value) ? response.data.courses.checked.filter(course => course !== value): response.data.courses.checked.concat([value]);
    const data = response.data;
    const courses = response.data.courses;

    setResponse({ ...response, data: { ...data, courses: { ...courses, checked: checked } } });
  };
}

const mapEditResponse = ({ user: { name, ...user }, ...data}) => {
  const mapEnrolledCourses = courses => courses.map(course => course.title);

  return {
    ...data,
    user: {
      ...user,
      firstName: name.first,
      lastName: name.last
    },
    courses: {
      data: mapCourseObjects(data.courses),
      checked: mapEnrolledCourses(user.enrolledCourses)
    }
  };
};

const mapNewResponse = data => {
  return {
    ...data,
    user: {
      firstName: "",
      lastName: "",
      email: "",
      username: "",
      password: ""
    },
    courses: {
      data: mapCourseObjects(data.courses),
      checked: []
    }
  }
};

const mapActiveUser = userId => {
  const mapActiveEnrolledCourses = userId => {
    return courses => {
      return courses.sort(sortCourses).map(course => {
        return {
          ...course,
          title: <Link key={course._id} to={`/users/${userId}/enrolled-courses/${course._id}`}>{course.title}</Link>
        };
      });
    };
  };

  return mapUserHOF(mapUsernameString, mapActiveEnrolledCourses(userId));
};

export { tableHeaders, mapUser, mapUsers, getCheckedCourseIds, mapCourseObjects, onInputChange, handleCheck, mapEditResponse, mapNewResponse, mapActiveUser };
