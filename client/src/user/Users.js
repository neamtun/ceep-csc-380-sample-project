import React from "react";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapUsers } from "./util";

const Users = () => {
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = "/api/users";
  const RESULT_FIELD = "users";

  const users = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapUsers);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Users</h3>

      {users.isLoading ?
        <Spinner animation="border" role="status"/> :
        users.errorMessage ?
          <p className="text-danger">{users.errorMessage}</p> :
          <Table headers={tableHeaders} rows={users.data} responsive="lg"/>}
    </div>
  );
};

export default Users;
