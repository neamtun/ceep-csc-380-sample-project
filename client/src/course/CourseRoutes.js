import React from "react";
import { Switch, useRouteMatch } from "react-router-dom";

import PrivateRoute from "../components/PrivateRoute";

import Course from "./Course";
import Courses from "./Courses";

const CourseRoutes = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute exact={true} path={path}>
        <Courses/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:courseId`}>
        <Course/>
      </PrivateRoute>
    </Switch>
  );
};

export default CourseRoutes;
