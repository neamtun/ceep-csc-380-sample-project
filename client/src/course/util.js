import React from "react";
import { Link } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import { sortStudyBooks } from "../StudyBook/util";

const tableHeaders = [
  { header: "Title", key: "title" },
  { header: "Description", key: "description" },
  { header: "Max Students", key: "maxStudents" },
  { header: "Study Books", key: "studyBooks" },
  { header: "Created At", key: "createdAt" },
  { header: "Updated At", key: "updatedAt" }
];

const mapStudyBooks = studyBooks => {
  return studyBooks.sort(sortStudyBooks).map(studyBook => <Row key={studyBook._id} ><Link to={`/study-books/${studyBook._id}`}>{studyBook.title}</Link></Row>);
};

const mapTitleString = course => course.title;

const mapTitleLink = course => <Link to={`/courses/${course._id}`}>{course.title}</Link>;

const mapCourseHOF = mapTitle => {
  return course => {
    return {
      ...course,
      title: mapTitle(course),
      studyBooks: <Container>{mapStudyBooks(course.studyBooks)}</Container>,
      createdAt: new Date(course.createdAt).toLocaleString(),
      updatedAt: new Date(course.updatedAt).toLocaleString()
    };
  };
};

const sortCourses = (course1, course2) => {
  if (course1.title > course2.title) {
    return 1;
  }
  else if (course1.title < course2.title) {
    return -1;
  }
  else {
    return 0;
  }
};

const mapCourse = mapCourseHOF(mapTitleString);

const mapCourses = courses => courses.sort(sortCourses).map(mapCourseHOF(mapTitleLink));

export { tableHeaders, mapCourse, mapCourses, sortCourses };
