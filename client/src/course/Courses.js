import React from "react";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapCourses } from "./util";

const Courses = () => {
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = "/api/courses";
  const RESULT_FIELD = "courses";

  const courses = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapCourses);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Courses</h3>

      {courses.isLoading ?
        <Spinner animation="border" role="status"/> :
        courses.errorMessage ?
          <p className="text-danger">{courses.errorMessage}</p> :
          <Table headers={tableHeaders} rows={courses.data} responsive="lg"/>}
    </div>
  );
};

export default Courses;
