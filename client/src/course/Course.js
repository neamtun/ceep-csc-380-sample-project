import React from "react";
import { useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapCourse } from "./util";

const Course = () => {
  const { courseId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/courses/${courseId}`;
  const RESULT_FIELD = "course";

  const course = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapCourse);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Course Page</h3>
      <h4 className="h4 font-weight-normal">{Object.keys(course.data).length > 0 ? `${course.data.title}:` : "..."}</h4>

      {course.isLoading ?
        <Spinner animation="border" role="status"/> :
        course.errorMessage ?
          <p className="text-danger">{course.errorMessage}</p> :
          <Table headers={tableHeaders} rows={Object.keys(course.data).length > 0 ? [course.data] : []} responsive="lg"/>}
    </div>
  )
}

export default Course;
