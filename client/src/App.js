import React from "react";
import { Route, Switch } from "react-router-dom";

import Container from "react-bootstrap/Container";

import Menu from "./components/Menu";
import PrivateRoute from "./components/PrivateRoute";

import CourseRoutes from "./Course/CourseRoutes";
import Home from "./Home/Home";
import QuestionRoutes from "./Question/QuestionRoutes";
import StudyBookRoutes from "./StudyBook/StudyBookRoutes";
import SubmissionRoutes from "./Submission/SubmissionRoutes";
import UserRoutes from "./User/UserRoutes";

import { AuthContext } from "./context/auth";

const App = () => {
  const existingUser = JSON.parse(localStorage.getItem("user")) || {};
  const [user, setUser] = React.useState(existingUser);

  const setLoggedInUser = (data) => {
    localStorage.setItem("user", JSON.stringify(data));
    setUser(data);
  }

  return (
    <AuthContext.Provider value={{ user, setUser: setLoggedInUser }}>
      <Container fluid className="h-100 p-0">
        <Menu/>
        <Switch>
          <PrivateRoute exact={true} path="/">
            <Home/>
          </PrivateRoute>
          <Route path="/courses">
            <CourseRoutes/>
          </Route>
          <Route path="/questions">
            <QuestionRoutes/>
          </Route>
          <Route path="/study-books">
            <StudyBookRoutes/>
          </Route>
          <Route path="/submissions">
            <SubmissionRoutes/>
          </Route>
          <Route path="/users">
            <UserRoutes/>
          </Route>
        </Switch>
      </Container>
    </AuthContext.Provider>
  )
};

export default App;
