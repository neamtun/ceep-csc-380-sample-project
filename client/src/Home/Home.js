import React from "react";

import ReferrerRedirect from "../components/ReferrerRedirect";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";

const Home = () => {
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = "/api";

  const status = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, "", (data) => data);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center text-center">
      <h3 className="h3 font-weight-normal pt-3">Welcome!</h3>
      <p>Please check out our study books to help learn computer science material.</p>

      {status.errorMessage && <p>{status.errorMessage}</p>}
    </div>
  )
};

export default Home;
