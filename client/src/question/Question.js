import React from "react";
import { useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { questionTableHeaders, submissionTableHeaders, mapResult } from "./util";

const Question = () => {
  const { questionId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/questions/${questionId}`;
  const RESULT_FIELD = "";

  const question = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapResult);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Question Page</h3>

      {question.isLoading ?
        <Spinner animation="border" role="status"/> :
        question.errorMessage ?
          <p className="text-danger">{question.errorMessage}</p> :
          <Table headers={questionTableHeaders} rows={Object.keys(question.data).length > 0 ? [question.data.question] : []} responsive="lg"/>}

      <h4 className="h4 font-weight-normal">Submissions:</h4>
      
      {question.isLoading ?
        <Spinner animation="border" role="status"/> :
        question.errorMessage ?
          <p className="text-danger">{question.errorMessage}</p> :
          <Table headers={submissionTableHeaders} rows={question.data.submissions ? question.data.submissions: []} responsive="md"/> }
    </div>
  );
};

export default Question;
