import React from "react";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { questionTableHeaders as tableHeaders, mapQuestions } from "./util";

const Questions = () => {
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = "/api/questions";
  const RESULT_FIELD = "questions";

  const questions = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapQuestions);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Questions</h3>

      {questions.isLoading ?
        <Spinner animation="border" role="status"/> :
        questions.errorMessage ?
          <p className="text-danger">{questions.errorMessage}</p> :
          <Table headers={tableHeaders} rows={questions.data} responsive="lg"/>}
    </div>
  );
};

export default Questions;
