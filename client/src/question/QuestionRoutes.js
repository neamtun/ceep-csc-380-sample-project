import React from "react";
import { Switch, useRouteMatch } from "react-router-dom";

import PrivateRoute from "../components/PrivateRoute";

import Question from "./Question";
import Questions from "./Questions";

const QuestionRoutes = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute exact={true} path={path}>
        <Questions/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:questionId`}>
        <Question/>
      </PrivateRoute>
    </Switch>
  )
};

export default QuestionRoutes;
