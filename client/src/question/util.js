import React from "react";
import { Link } from "react-router-dom";

import { sortByUser } from "../Submission/util"

const questionTableHeaders = [
  { header: "Text", key: "text"},
  { header: "Answer", key: "answer"},
  { header: "Created At", key: "createdAt"},
  { header: "Updated At", key: "updatedAt"}
];

const submissionTableHeaders = [
 { header: "Response", key: "response" },
 { header: "User", key: "user" }
];

const mapSubmissions = submissions => {
  return submissions.sort(sortByUser).map(({ user, ...submission }) => {
    return {
      ...submission,
      user: <Link to={`/users/${user._id}`}>{user.username}</Link>,
      response: <Link to={`/submissions/${submission._id}`}>{submission.response}</Link>
    };
  });
};

const sortQuestions = (question1, question2) => {
  if (question1.text > question2.text) {
    return 1;
  }
  else if (question1.text < question2.text) {
    return -1;
  }
  else {
    return 0;
  }
};

const mapResult = ({ question, submissions }) => {
  return {
    question: {
      ...question,
      createdAt: new Date(question.createdAt).toLocaleString(),
      updatedAt: new Date(question.updatedAt).toLocaleString()
    },
    submissions: mapSubmissions(submissions),
  };
};

const mapQuestions = questions => {
  return questions.sort(sortQuestions).map(question => {
    return {
      ...question,
      text: <Link to={`/questions/${question._id}`}>{question.text}</Link>,
      createdAt: new Date(question.createdAt).toLocaleString(),
      updatedAt: new Date(question.updatedAt).toLocaleString()
    };
  });
};

export { questionTableHeaders, submissionTableHeaders, mapResult, mapQuestions, sortQuestions };
