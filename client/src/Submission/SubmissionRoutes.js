import React from "react";
import { Switch, useRouteMatch } from "react-router-dom";

import PrivateRoute from "../components/PrivateRoute";

import Submission from "./Submission";
import Submissions from "./Submissions";

const SubmissionRoutes = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute exact={true} path={path}>
        <Submissions/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:submissionId`}>
        <Submission/>
      </PrivateRoute>
    </Switch>
  );
};

export default SubmissionRoutes;
