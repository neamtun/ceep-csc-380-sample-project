import React from "react";
import { Link } from "react-router-dom";

const tableHeaders = [
  { header: "Response", key: "response"},
  { header: "Correct Answer", key: "correctAnswer"},
  { header: "Question", key: "question"},
  { header: "User", key: "user"},
  { header: "Rating", key: "rating"},
  { header: "Created At", key: "createdAt"},
  { header: "Updated At", key: "updatedAt"}
];

const mapResponseString = submission => submission.response;

const mapResponseLink = submission => <Link to={`/submissions/${submission._id}`}>{submission.response}</Link>;

const ratingClass = rating => {
  if (rating < 2) {
    return "text-danger";
  }
  else if (rating < 4) {
    return "text-warning";
  }
  else {
    return "text-success";
  }
};

const mapSubmissionHOF = mapResponse => {
  return ({ user, question, ...submission }) => {
    return {
      ...submission,
      user: <Link to={`/users/${user._id}`}>{user.username}</Link>,
      question: <Link to={`/questions/${question._id}`}>{question.text}</Link>,
      correctAnswer: question.answer,
      response: mapResponse(submission),
      createdAt: new Date(submission.createdAt).toLocaleString(),
      updatedAt: new Date(submission.updatedAt).toLocaleString(),
      rating: <div className={ratingClass(submission.rating)}>{`${submission.rating}`}</div>
    };
  };
};

const sortByUser = (submission1, submission2) => {
  if (submission1.user.username > submission2.user.username) {
    return 1;
  }
  else if (submission1.user.username < submission2.user.username) {
    return -1;
  }
  else {
    return 0;
  }
};

const sortByQuestion = (submission1, submission2) => {
  if (submission1.question.text > submission2.question.text) {
    return 1;
  }
  else if (submission1.question.text < submission2.question.text) {
    return -1;
  }
  else {
    return 0;
  }
};

const mapSubmission = mapSubmissionHOF(mapResponseString);

const mapSubmissions = submissions => submissions.sort(sortByUser).sort(sortByQuestion).map(mapSubmissionHOF(mapResponseLink));

export { tableHeaders, mapSubmission, mapSubmissions, sortByUser, ratingClass };
