import React from "react";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapSubmissions } from "./util";

const Submissions = () => {
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = "/api/submissions";
  const RESULT_FIELD = "submissions";

  const submissions = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapSubmissions);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Submissions</h3>

      {submissions.isLoading ?
        <Spinner animation="border" role="status"/> :
        submissions.errorMessage ?
          <p className="text-danger">{submissions.errorMessage}</p> :
          <Table headers={tableHeaders} rows={submissions.data} responsive="lg"/> }
    </div>
  );
};

export default Submissions;
