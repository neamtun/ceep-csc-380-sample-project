import React from "react";
import { useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapSubmission } from "./util";

const Submission = () => {
  const { submissionId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/submissions/${submissionId}`;
  const RESULT_FIELD = "submission";

  const submission = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapSubmission);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Submission Page</h3>

      {submission.isLoading ?
        <Spinner animation="border" role="status"/> :
        submission.errorMessage ?
          <p className="text-danger">{submission.errorMessage}</p> :
          <Table headers={tableHeaders} rows={Object.keys(submission.data).length > 0 ? [submission.data] : []} responsive="lg"/>}
    </div>
  )
};

export default Submission;
