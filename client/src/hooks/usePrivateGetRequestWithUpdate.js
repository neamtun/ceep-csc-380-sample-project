import React from "react";

import { getRequest } from "./util";

const usePrivateGetRequestWithUpdate = (setLoggedIn, apiEndpoint, resultField, mapResult) => {
  const [response, setResponse] = React.useState({ data: [], isLoading: false, errorMessage: "" });

  React.useEffect(() => {
    getRequest(setLoggedIn, apiEndpoint, resultField, mapResult, setResponse);
  }, [setLoggedIn, apiEndpoint, resultField, mapResult, setResponse]);

  return [response, setResponse];
};

export default usePrivateGetRequestWithUpdate;
