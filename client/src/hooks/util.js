import axios from "axios";

const getRequest = (setLoggedIn, apiEndpoint, resultField, mapResult, setResponse) => {
  setResponse(response => {
    return { ...response, isLoading: true, errorMessage: "" };
  });

  axios.get(apiEndpoint)
    .then(result => {
      if (! result.data.data.loggedIn) {
        setLoggedIn(false);
      }
      else if (result.data.status === 500) {
        setResponse(response => {
          return { ...response, isLoading: false, errorMessage: result.data.message };
        });
      }
      else {
        const resData = resultField === "" ? result.data.data : result.data.data[resultField];

        setResponse(response => {
            return {
            ...response,
            data: mapResult(resData),
            isLoading: false,
            errorMessage: ""
          };
        });
      }
    })
    .catch(() =>
      setResponse(response => {
        return { ...response, isLoading: false, errorMessage: "Something went wrong..." };
      })
    );
};

export { getRequest };
