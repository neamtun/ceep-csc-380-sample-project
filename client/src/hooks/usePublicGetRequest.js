import React from "react";
import axios from "axios";

import useEffectOnlyOnce from "./useEffectOnlyOnce";

const usePublicGetRequest = (apiEndpoint, resultField, mapResult) => {
  const [response, setResponse] = React.useState({ data: [], isLoading: false, errorMessage: "" });

  useEffectOnlyOnce(() => {
    setResponse({ ...response, isLoading: true, errorMessage: "" });

    axios.get(apiEndpoint)
      .then(result => {
        if (result.data.status === 500) {
          setResponse({ ...response, isLoading: false, errorMessage: result.data.message });
        }
        else {
          const resData = resultField === "" ? result.data.data : result.data.data[resultField];

          setResponse({
            ...response,
            data: mapResult(resData),
            isLoading: false,
            errorMessage: ""
          });
        }
      })
      .catch(() =>
        setResponse({ ...response, isLoading: false, errorMessage: "Something went wrong..." })
      );
  });

  return [response, setResponse];
};

export default usePublicGetRequest;
