import React from "react";

const useEffectOnlyOnce = (func) => React.useEffect(func, []);

export default useEffectOnlyOnce;
