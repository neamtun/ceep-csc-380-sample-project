import usePrivateGetRequestWithSetter from "./usePrivateGetRequestWithSetter"

const usePrivateGetRequest = (setLoggedIn, apiEndpoint, resultField, mapResult) => {
  return usePrivateGetRequestWithSetter(setLoggedIn, apiEndpoint, resultField, mapResult)[0];
};

export default usePrivateGetRequest;
