import React from "react";

import { useAuth } from "../context/auth";

const useLoggedIn = () => {
  const { user, setUser } = useAuth();
  const [loggedIn, setLoggedIn] = React.useState(user.loggedIn);

  React.useEffect(() => {
    if (! loggedIn) {
      setUser( {} );
    }
  }, [loggedIn, setUser]);

  return [loggedIn, setLoggedIn];
};

export default useLoggedIn;
