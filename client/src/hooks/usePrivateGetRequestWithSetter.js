import React from "react";

import useEffectOnlyOnce from "./useEffectOnlyOnce";
import { getRequest } from "./util";

const usePrivateGetRequestWithSetter = (setLoggedIn, apiEndpoint, resultField, mapResult) => {
  const [response, setResponse] = React.useState({ data: [], isLoading: false, errorMessage: "" });

  useEffectOnlyOnce(() => {
    getRequest(setLoggedIn, apiEndpoint, resultField, mapResult, setResponse);
  });

  return [response, setResponse];
};

export default usePrivateGetRequestWithSetter;
