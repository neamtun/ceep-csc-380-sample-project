import React from "react";

const AuthContext = React.createContext();

const useAuth = () => {
  return React.useContext(AuthContext);
};

export { AuthContext, useAuth };
