import React from "react";
import { Link } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import { sortQuestions } from "../Question/util";

const tableHeaders = [
  { header: "Title", key: "title"},
  { header: "Questions", key: "questions"},
  { header: "Created At", key: "createdAt"},
  { header: "Updated At", key: "updatedAt"}
];

const mapQuestions = questions => {
  return questions.sort(sortQuestions).map(question => <Row key={question._id} ><Link to={`/questions/${question._id}`}>{question.text}</Link></Row>);
};

const mapTitleString = studyBook => studyBook.title;

const mapTitleLink = studyBook => <Link to={`/study-books/${studyBook._id}`}>{studyBook.title}</Link>;

const mapStudyBookHOF = mapTitle => {
  return studyBook => {
    return {
      ...studyBook,
      title: mapTitle(studyBook),
      questions: <Container>{mapQuestions(studyBook.questions)}</Container>,
      createdAt: new Date(studyBook.createdAt).toLocaleString(),
      updatedAt: new Date(studyBook.updatedAt).toLocaleString()
    };
  };
};

const sortStudyBooks = (studyBook1, studyBook2) => {
  if (studyBook1.title > studyBook2.title) {
    return 1;
  }
  else if (studyBook1.title < studyBook2.title) {
    return -1;
  }
  else {
    return 0;
  }
};

const mapStudyBook = mapStudyBookHOF(mapTitleString);

const mapStudyBooks = studyBooks => studyBooks.sort(sortStudyBooks).map(mapStudyBookHOF(mapTitleLink));

export { tableHeaders, mapQuestions, mapStudyBook, mapStudyBooks, sortStudyBooks };
