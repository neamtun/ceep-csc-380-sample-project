import React from "react";
import { useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapStudyBook } from "./util";

const StudyBook = () => {
  const { studyBookId } = useParams();
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = `/api/study-books/${studyBookId}`
  const RESULT_FIELD = "studyBook";

  const studyBook = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapStudyBook);

  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Study Book Page</h3>
      <h4 className="h4 font-weight-normal">{Object.keys(studyBook.data).length > 0 ? `${studyBook.data.title}:` : "..."}</h4>

      {studyBook.isLoading ?
        <Spinner animation="border" role="status"/> :
        studyBook.errorMessage ?
          <p className="text-danger">{studyBook.errorMessage}</p> :
          <Table headers={tableHeaders} rows={Object.keys(studyBook.data).length > 0 ? [studyBook.data] : []} responsive="lg"/>}
    </div>
  );
};

export default StudyBook;
