import React from "react";

import Spinner from "react-bootstrap/Spinner";

import ReferrerRedirect from "../components/ReferrerRedirect";
import Table from "../components/Table";
import useLoggedIn from "../hooks/useLoggedIn";
import usePrivateGetRequest from "../hooks/usePrivateGetRequest";
import { tableHeaders, mapStudyBooks } from "./util";

const StudyBooks = () => {
  const [loggedIn, setLoggedIn] = useLoggedIn();

  const API_ENDPOINT = "/api/study-books";
  const RESULT_FIELD = "studyBooks";

  const studyBooks = usePrivateGetRequest(setLoggedIn, API_ENDPOINT, RESULT_FIELD, mapStudyBooks);


  if (! loggedIn) {
    return <ReferrerRedirect to="/users/login"/>;
  }

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <h3 className="h3 font-weight-normal pt-3">Admin Study Books</h3>

      {studyBooks.isLoading ?
        <Spinner animation="border" role="status"/> :
        studyBooks.errorMessage ?
          <p className="text-danger">{studyBooks.errorMessage}</p> :
          <Table headers={tableHeaders} rows={studyBooks.data} responsive="lg"/>}
    </div>
  );
};

export default StudyBooks;
