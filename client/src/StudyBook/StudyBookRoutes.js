import React from "react";
import { Switch, useRouteMatch } from "react-router-dom";

import PrivateRoute from "../components/PrivateRoute";

import StudyBook from "./StudyBook";
import StudyBooks from "./StudyBooks"

const StudyBookRoutes = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <PrivateRoute exact={true} path={path}>
        <StudyBooks/>
      </PrivateRoute>
      <PrivateRoute path={`${path}/:studyBookId`}>
        <StudyBook/>
      </PrivateRoute>
    </Switch>
  )
};

export default StudyBookRoutes;
