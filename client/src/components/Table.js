import React from "react";

import BootstrapTable from "react-bootstrap/Table"

const Table = ({ headers, rows, ...others}) => (
  <BootstrapTable bordered striped {...others}>
    <thead>
      <tr>
        {headers.map(h => <th key={h.header}>{h.header}</th>)}
      </tr>
    </thead>
    <tbody>
      {rows.map(row =>
        <tr key={row._id}>
          {headers.map(h => <td className="td" key={h.header}>{row[h.key]}</td>)}
        </tr>
      )}
    </tbody>
  </BootstrapTable>
);

export default Table;
