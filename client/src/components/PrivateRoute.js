import React from "react";
import { Route } from "react-router-dom";

import ReferrerRedirect from "../components/ReferrerRedirect";
import { useAuth } from "../context/auth";

const PrivateRoute = ({ children, ...rest }) => {
  const { user } = useAuth();

  return (
    <Route {...rest}>
      {user.loggedIn ?
        (children)
        :
        (<ReferrerRedirect to="/users/login"/>)
      }
    </Route>
  );
};

export default PrivateRoute;
