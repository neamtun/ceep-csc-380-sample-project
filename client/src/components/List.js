import React from "react";

const List = ({ list }) => (
  <>
    {list.map(({ _id, ...item }) => {
      return (
        <div key={_id}>
          {Object.keys(item)
            .map(key =>
                <span key={key}>{item[key]} </span>
            )
          }
        </div>)
    })}
  </>
);

export default List;
