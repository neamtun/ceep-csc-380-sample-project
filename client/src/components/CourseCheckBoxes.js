import React from "react";

import Form from "react-bootstrap/Form";

const CourseCheckBox = ({ courses, course, name, onCheck }) => (
  <Form.Row className="justify-content-center">
    <Form.Check name={name} value={course.title} checked={courses.checked.includes(course.title)} onChange={onCheck}/>
    <Form.Label htmlFor={course.title}>{course.title}</Form.Label>
  </Form.Row>
);

const CourseCheckBoxes = ({ courses, onCheck, children }) => {
  const name = "courses";

  return  (
    <>
      <Form.Label htmlFor={name}>{children}</Form.Label>
      {courses.data ?
        courses.data.map(course => {
          return (
            <CourseCheckBox key={course._id} courses={courses} course={course} name={courses} onCheck={onCheck}/>
          )
        }) :
        "..."}
    </>
  )
};

export default CourseCheckBoxes;
