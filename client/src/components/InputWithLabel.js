import React from "react";

const InputWithLabel = ({ id, value, type = "text", onInputChange, isFocused = false, children, ...others}) => (
  <>
    <label htmlFor={id}>{children}</label>
    <br/>
    {type === "textarea" ?
      <textarea id={id} value={value} onChange={onInputChange} autoFocus={isFocused} {...others}/> :
      <input id={id} type={type} value={value} onChange={onInputChange} autoFocus={isFocused} {...others}/>
    }
  </>
);

export default InputWithLabel;
