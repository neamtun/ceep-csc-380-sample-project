import React from "react";
import { Redirect, useRouteMatch } from "react-router-dom";

const ReferrerRedirect = ({ to }) => {
  const { url } = useRouteMatch();
  const redirect = {
    pathname: to,
    state: { referrer: url }
  };

  return (
    <Redirect to={redirect}/>
  )
}

export default ReferrerRedirect;
