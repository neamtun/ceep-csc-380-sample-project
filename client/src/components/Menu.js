import React from "react";
import { Link } from "react-router-dom";

import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";

import { useAuth } from "../context/auth";

const Menu = () => {
  const { user } = useAuth();

  return (
    <Navbar bg="dark" variant="dark" collapseOnSelect expand="sm" className="h-10">
      <Navbar.Brand>Study Book</Navbar.Brand>
      {user.loggedIn ?
        <>
          <Navbar.Toggle/>
          <Navbar.Collapse>
            <Nav className="mr-auto">
              <Nav.Item className="mr-2" as={Link} to="/">
                <Navbar.Text>
                  Home
                </Navbar.Text>
              </Nav.Item>
              <Nav.Item as={Link} to={`/users/${user._id}/enrolled-courses`}>
                <Navbar.Text>
                  Enrolled Courses
                </Navbar.Text>
              </Nav.Item>
              <NavDropdown title="Admin">
                <NavDropdown.Item as={Link} to="/users">
                  Users
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/courses">
                  Courses
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/study-books">
                  Study Books
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/questions">
                  Questions
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/submissions">
                  Submissions
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
              <Nav.Item className="mr-2" as={Link} to={`/users/${user._id}`}>
                <Navbar.Text>
                  {`${user.username}`}
                </Navbar.Text>
              </Nav.Item>
              <Nav.Item>
                <Button variant="outline-secondary" as={Link} to="/users/logout">Logout</Button>
              </Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </> :
        <></>
      }
    </Navbar>
  );
};

export default Menu;
