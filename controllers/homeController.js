"use strict"

const httpStatus = require("http-status-codes");

module.exports = {
  index: (req, res) => {
    res.render("index");
  },

  courses: (req, res) => {
    res.render("courses", {
      courses: courses
    })
  },

  redirect: (req, res, next) => {
    let redirectPath = res.locals.redirect;

    if (redirectPath !== undefined) {
      res.redirect(redirectPath);
    }
    else {
      next();
    }
  },

  redirectAsGet: (req, res, next) => {
    let redirectPath = res.locals.redirect;

    if (redirectPath !== undefined) {
      res.redirect(303, redirectPath);
    }
    else {
      next();
    }
  },

  respondJSON: (req, res) => {
    res.json({
      status: httpStatus.OK,
      data: res.locals
    });
  }
};
