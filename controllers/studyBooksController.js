"use strict"

const StudyBook = require("../models/studyBook"),

  validateActiveQuestionHelper = (req, res, next, redirect) => {
    if (req.skip) {
      return next();
    }

    let studyBookId = req.params.studyBookId,
      questionId = req.params.questionId;

    StudyBook.findById(studyBookId)
      .populate("questions")
      .then(studyBook => {
        const question = studyBook.questions.find(q => q._id == questionId);

        if (question !== undefined) {
          res.locals.question = question;
          next()
        }
        else {
          req.skip = true;
          let userId = req.params.userId,
            courseId = req.params.questionId;
          req.flash("error", "Question is invalid for selected study book");
          res.locals.redirect = redirect;
          next();
        }
      })
      .catch(error => {
        console.log(`Error validating user enrollment: ${error.message}`);
        next(error);
      });
  },

  nextActiveQuestionHelper = (req, res, next, redirectPath) => {
    if (req.skip) {
      return next();
    }

    let questions = res.locals.studyBook.questions,
      currentQuestionId = res.locals.question._id;

    let nextQuestionIdx = questions.map(q => q._id).indexOf(currentQuestionId) + 1;

    if (nextQuestionIdx < questions.length) {
      res.locals.question = questions[nextQuestionIdx];
      next();
    }
    else {
      let userId = req.params.userId,
        courseId = req.params.courseId,
        studyBookId = req.params.studyBookId;

      res.locals.redirect = redirectPath;
      next();
    }
  };

module.exports = {
  all: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    StudyBook.find()
      .populate("questions")
      .then(studyBooks => {
        res.locals.studyBooks = studyBooks.sort(module.exports.sortStudyBooks);
        next();
      })
      .catch(error => {
        console.log(`Error fetching study books: ${error.message}`);
        next(error);
      });
  },

  indexView: (req, res) => {
    if (! req.skip) {
      res.render("studyBooks/index");
    }
  },

  show: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let studyBookId = req.params.studyBookId;
    StudyBook.findById(studyBookId)
      .populate("questions")
      .then(studyBook => {
        res.locals.studyBook = studyBook;
        next();
      })
      .catch(error => {
        console.log(`Error fetching study book by ID: ${error.message}`);
        next(error);
      })
  },

  showView: (req, res) => {
    if (! req.skip) {
      res.render("studyBooks/show");
    }
  },

  validateActiveQuestion: (req, res, next) => {
    let userId = req.params.userId,
      courseId = req.params.courseId;

    validateActiveQuestionHelper(req, res, next, `/users/${userId}/enrolled-courses/${courseId}`);
  },

  validateActiveQuestionAPI: (req, res, next) => {
    let userId = req.params.userId,
      courseId = req.params.courseId;

    validateActiveQuestionHelper(req, res, next, `/api/users/${userId}/enrolled-courses/${courseId}`);
  },

  nextActiveQuestion: (req, res, next) => {
    let userId = req.params.userId,
      courseId = req.params.courseId,
      studyBookId = req.params.studyBookId;

    nextActiveQuestionHelper(req, res, next, `/users/${userId}/enrolled-courses/${courseId}/active-study-book/${studyBookId}/previous-submission`);
  },

  nextActiveQuestionAPI: (req, res, next) => {
    let userId = req.params.userId,
      courseId = req.params.courseId,
      studyBookId = req.params.studyBookId;

    nextActiveQuestionHelper(req, res, next, `/api/users/${userId}/enrolled-courses/${courseId}/active-study-book/${studyBookId}/previous-submission`);
  },

  sortStudyBooks: (bookA, bookB) => {
    if (bookA.title > bookB.title)
      return 1;
    else if (bookA.title < bookB.title)
      return -1;
    else
      return 0;
  }
}
