"use strict"

const Submission = require("../models/submission"),

  getSubmissionParams = body => {
    return {
      response: body.response,
      rating: body.rating
    };
  };

module.exports = {
  all: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    Submission.find()
      .populate("user")
      .populate("question")
      .then(submissions => {
        res.locals.submissions = submissions;
        next();
      })
      .catch(error => {
        console.log(`Error fetching submissions: ${error.message}`);
        next(error);
      });
  },

  indexView: (req, res) => {
    if (! req.skip) {
      res.render("submissions/index");
    }
  },

  show: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let submissionId = req.params.submissionId;
    Submission.findById(submissionId)
      .populate("user")
      .populate("question")
      .then(submission => {
        res.locals.submission = submission;
        next();
      })
      .catch(error => {
        console.log(`Error fetching submission by ID: ${error.message}`);
        next(error);
      });
  },

  showView: (req, res) => {
    if (! req.skip) {
      res.render("submissions/show");
    }
  },

  previousSubmission: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let userId = res.locals.currentUser._id,
     questionIds = res.locals.studyBook.questions.map(q => q._id);

    Submission.find({ user: userId, question: { $in: questionIds} })
      .populate("user")
      .populate("question")
      .then(submissions => {
        res.locals.submissions = submissions;
        next();
      })
      .catch(error => {
        console.log(`Error finding previous submission: ${error.message}`);
        next(error);
      });
  },

  previousSubmissionView: (req, res, next) => {
    if (! req.skip) {
      res.render("submissions/previousSubmission");
    }
  },

  submitActiveResponse: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId,
      questionId = req.params.questionId,
      submissionParams = getSubmissionParams(req.body);

    Submission.findOneAndUpdate({ user: userId, question: questionId }, {
        $set: submissionParams
      })
      .then(submission => {
        // If submission document does not exist then the update will return null
        // and a submision document must be created
        if (submission == null) {
          Submission.create({
              response: submissionParams.response,
              rating: submissionParams.rating,
              user: userId,
              question: questionId
            })
            .then(() => {
              next();
            })
        }
        else {
          next();
        }
      })
      .catch(error => {
        console.log(`Error submitting active response: ${error.message}`);
        next(error);
      });
  },

  deleteByUserId: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId;

    Submission.deleteMany({ user: userId })
      .then(() => {
        next();
      })
      .catch(error => {
        console.log(`Error deleting submissions by user ID: ${error.message}`);
        next(error);
      });
  }
};
