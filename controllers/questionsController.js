"use strict"

const Question = require("../models/question"),
  Submission = require("../models/submission");

module.exports = {
  all: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    Question.find()
      .then(questions => {
        res.locals.questions = questions;
        next();
      })
      .catch(error => {
        console.log(`Error fetching questions: ${error.message}`);
        next(error);
      });
  },

  indexView: (req, res) => {
    if (! req.skip) {
      res.render("questions/index");
    }
  },

  show: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let questionId = req.params.questionId;
    Question.findById(questionId)
      .then(question => {
        res.locals.question = question;
        Submission.find({ question: question._id })
        .populate("user")
          .then(submissions => {
            res.locals.submissions = submissions;
            next();
          });
      })
      .catch(error => {
        console.log(`Error fetching question by ID: ${error.message}`);
        next(error);
      });
  },

  showView: (req, res) => {
    if (! req.skip) {
      res.render("questions/show");
    }
  },

  activeQuestionView: (req, res) => {
    if (! req.skip) {
      res.render("questions/activeQuestion");
    }
  }
}
