"use strict"

const Course = require("../models/course"),
{ sortStudyBooks } = require("./studyBooksController"),

  validateActiveStudyBookHelper = (req, res, next, redirectPath) => {
    if (req.skip) {
      return next();
    }

    let courseId = req.params.courseId,
      studyBookId = req.params.studyBookId;

    Course.findById(courseId)
      .populate({
        path: "studyBooks",
        populate: {
          path: "questions"
        }
      })
      .then(course => {
        const studyBook = course.studyBooks.find(sb => sb._id == studyBookId);

        if (studyBook !== undefined) {
          res.locals.studyBook = studyBook;
          next()
        }
        else {
          req.skip = true;
          let userId = req.params.userId;
          req.flash("error", "Study book is invalid for selected course");
          res.locals.redirect = redirectPath;
          next();
        }
      })
      .catch(error => {
        console.log(`Error validating user enrollment: ${error.message}`);
        next(error);
      });
  };


module.exports = {
  all: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    Course.find()
      .populate("studyBooks")
      .then(courses => {
        res.locals.courses = courses;
        next();
      })
      .catch(error => {
        console.log(`Error fetching courses: ${error.message}`);
        next(error);
      });
  },

  indexView: (req, res) => {
    if (! req.skip) {
      res.render("courses/index");
    }
  },

  show: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let courseId = req.params.courseId;
    Course.findById(courseId)
      .populate("studyBooks")
      .then(course => {
        course.studyBooks.sort(sortStudyBooks);
        res.locals.course = course;
        next();
      })
      .catch(error => {
        console.log(`Error fetching course by ID: ${error.message}`);
        next(error);
      });
  },

  showView: (req, res) => {
    if (! req.skip) {
      res.render("courses/show");
    }
  },

  showEnrolledCoursesView: (req, res) => {
    if (! req.skip) {
      res.render("courses/showEnrolledCourses");
    }
  },

  validateActiveStudyBook: (req, res, next) => {
    let userId = req.params.userId,
      courseId = req.params.courseId;

    validateActiveStudyBookHelper(req, res, next, `/users/${userId}/enrolled-courses/${courseId}`);
  },

  validateActiveStudyBookAPI: (req, res, next) => {
    let userId = req.params.userId,
      courseId = req.params.courseId;

    validateActiveStudyBookHelper(req, res, next, `/api/users/${userId}/enrolled-courses/${courseId}`);
  },

  sortCourses: (courseA, courseB) => {
    if (courseA.title > courseB.title)
      return 1;
    else if (courseA.title < courseB.title)
      return -1;
    else
      return 0;
  }
}
