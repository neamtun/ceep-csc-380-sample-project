"use strict"

const httpStatus = require("http-status-codes");

module.exports = {

  pageNotFoundError: (req, res) => {
    let url = req.url;
    let errorCode = httpStatus.NOT_FOUND;

    if (url.includes("api")) {
      let errorObject = {
        status: errorCode,
        message: "Oops! Somehing went wrong, or the page you are looking for is not available."
      };

      res.json(errorObject);
    }
    else {
      res.status(errorCode);

      res.render("error", {
        errorCode: errorCode
      });
    }
  },

  internalServerError: (error, req, res, next) => {
    let url = req.url;
    let errorCode = httpStatus.INTERNAL_SERVER_ERROR;
    let message = `${errorCode} | Sorry, our application is experiencing a problem!`;

    if (url.includes("api")) {
      let errorObject = {
        status: errorCode,
        message: message
      };

      res.json(errorObject);
    }
    else{
      res.status(errorCode);
      res.send(`${errorCode} | Sorry, our application is experiencing a problem!`);
    }
  }
}
