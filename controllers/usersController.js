"use strict"

const User = require("../models/user"),
  Course = require("../models/course"),
  { sortCourses } = require("./coursesController"),
  passport = require("passport"),
  { check, validationResult } = require("express-validator"),

  getUserParams = body => {
    return {
      name: {
        first: body.firstName,
        last: body.lastName
      },
      email: body.email,
      username: body.username,
      enrolledCourses: body.enrolledCourses
    };
  },

  createHelper = (req, res, next, redirectPath) => {
    if (req.skip) {
      return next();
    }

    let newUser = new User(getUserParams(req.body));

    User.register(newUser, req.body.password, (e, user) => {
      if (user) {
        req.flash("success", `${user.username}'s account created successfully!`);
        next();
      }
      else {
        req.flash("error", `Failed to create user account because: ${e.message}`);
        res.locals.redirect = redirectPath;
        next();
      }
    });
  },

  editHelper = (req, res, next, isAPI) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId;
    User.findById(userId)
      .populate("enrolledCourses")
      .then(user => {
        if (isAPI) {
          res.locals.user = user;
          next();
        }
        else {
          res.render("users/edit", {
            user: user
          });
        }
      })
      .catch(error => {
        console.log(`Error fetching user by ID: ${error.message}`);
        next(error);
      });
  },

  updateHelper = (req, res, next, redirectPath) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId,
      userParams = getUserParams(req.body);

    User.findByIdAndUpdate(userId, {
        $set: userParams
      },
      {
        new: true
      })
      .then(user => {
        res.locals.redirect = redirectPath;
        next();
      })
      .catch(error => {
        console.log(`Error updating user by ID: ${error.message}`);
        next(error);
      });
  },

  deleteHelper = (req, res, next, redirectPath) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId;

    User.findByIdAndRemove(userId)
      .then(() => {
        res.locals.redirect = redirectPath;
        next();
      })
      .catch(error => {
        console.log(`Error deleting user by ID: ${error.message}`);
        next(error);
      });
  },

  validateHelper = (req, res, next, redirectPath) => {
    check("email", "Email is invalid")
      .normalizeEmail({
        all_lowercase: true
      })
      .trim()
      .isEmail();

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      let messages = errors.array().map(e => e.msg);
      req.skip = true;
      req.flash("error", message.join(" and "));
      res.locals.redirect = redirectPath;
      next();
    }
    else {
      next();
    }
  },

  loggedInHelper = (req, res, next, redirectPath) => {
    if (! res.locals.loggedIn) {
      req.skip = true;
      req.flash("error", "You must be logged in to access that page.");
      res.locals.redirect = redirectPath;
    }

    next();
  },

  logoutHelper = (req, res, next, redirectPath) => {
    req.logout();
    req.flash("success", "You have been logged out!");
    res.locals.redirect = redirectPath;
    next();
  },

  validateEnrolledHelper = (req, res, next, redirectPath) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId,
      courseId = req.params.courseId;

    User.findById(userId)
      .populate("enrolledCourses")
      .then(user => {
        const course = user.enrolledCourses.find(c => c._id == courseId);

        if (course !== undefined) {
          res.locals.course = course;
          next();
        }
        else {
          req.skip = true;
          req.flash("error", "User is not enrolled in that class");
          res.locals.redirect = redirectPath;
          next();
        }
      })
      .catch(error => {
        console.log(`Error validating user enrollment: ${error.message}`);
        next(error);
      });
  };

module.exports = {
  loggedIn: (req, res, next) => {
    loggedInHelper(req, res, next, "/users/login")
  },

  loggedInAPI: (req, res, next) => {
    loggedInHelper(req, res, next, "/api/users/login")
  },

  all: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    User.find()
      .populate("enrolledCourses")
      .then(users => {
        res.locals.users = users;
        next();
      })
      .catch(error => {
        console.log(`Error fetching users: ${error.message}`);
        next(error);
      });
  },

  indexView: (req, res) => {
    if (! req.skip) {
      res.render("users/index");
    }
  },

  new: (req, res) => {
    res.render("users/new");
  },

  create: (req, res, next) => {
    createHelper(req, res, next, "/users/new");
  },

  createAPI: (req, res, next) => {
    createHelper(req, res, next, "/api/users/new");
  },

  show: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId;
    User.findById(userId)
      .populate("enrolledCourses")
      .then(user => {
        res.locals.user = user;
        next();
      })
      .catch(error => {
        console.log(`Error fetching user by ID: ${error.message}`);
        next(error);
      });
  },

  showView: (req, res) => {
    if (! req.skip) {
      res.render("users/show");
    }
  },

  edit: (req, res, next) => {
    editHelper(req, res, next, false);
  },

  editAPI: (req, res, next) => {
    editHelper(req, res, next, true);
  },

  update: (req, res, next) => {
    let userId = req.params.userId;

    updateHelper(req, res, next, `/users/${userId}`);
  },

  updateAPI: (req, res, next) => {
    let userId = req.params.userId;

    updateHelper(req, res, next, `/api/users/${userId}`);
  },

  delete: (req, res, next) => {
    deleteHelper(req, res, next, "/users");
  },

  deleteAPI: (req, res, next) => {
    deleteHelper(req, res, next, "/api/users");
  },

  login: (req, res) => {
    res.render("users/login");
  },

  indexEnrolledCourses: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    let userId = req.params.userId;
    User.findById(userId)
      .populate("enrolledCourses")
      .then(user => {
        res.locals.user = user;
        next();
      })
      .catch(error => {
        console.log(`Error fetching user by ID: ${error.message}`);
        next(error);
      });
  },

  indexEnrolledCoursesView: (req, res) => {
    if (! req.skip) {
      res.render("users/indexEnrolledCourses");
    }
  },

  validateEnrolled: (req, res, next) => {
    let userId = req.params.userId;

    validateEnrolledHelper(req, res, next, `/users/${userId}/enrolled-courses`);
  },

  validateEnrolledAPI: (req, res, next) => {
    let userId = req.params.userId;

    validateEnrolledHelper(req, res, next, `/api/users/${userId}/enrolled-courses`);
  },

  validate: (req, res, next) => {
    validateHelper(req, res, next, "/users/new");
  },

  validateAPI: (req, res, next) => {
    validateHelper(req, res, next, "/api/users/new");
  },

  authenticate: passport.authenticate("local", {
    failureRedirect: "/users/login",
    failureFlash: "Failed to login.",
    successRedirect: "/",
    successFlash: "Logged in!"
  }),

  authenticateAPI: passport.authenticate("local", {
    failureRedirect: "/api/users/login",
    failureFlash: "Failed to login.",
    successRedirect: "/api",
    successFlash: "Logged in!"
  }),

  logout: (req, res, next) => {
    logoutHelper(req, res, next, "/users/login");
  },

  logoutAPI: (req, res, next) => {
    logoutHelper(req, res, next, "/api/users/login");
  }
};
